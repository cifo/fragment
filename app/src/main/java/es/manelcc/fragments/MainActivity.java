package es.manelcc.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnFragmentInteractionListener {


    private TextView nombre;
    private Button btn, btnRespuesta;
    private EditText respuesta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //1 Iniciar witcheds

        initComponents();


    }


    /**
     * Call for init widchets and listeners.
     */

    private void initComponents() {

        //Texto donde se muestra el nombre que proviene del Fragment.
        nombre = (TextView) findViewById(R.id.mainActivity_txtNombre);

        //Boton para iniciar el fragment
        btn = (Button) findViewById(R.id.mainActivity_btn);
        btn.setOnClickListener(this);

        //Editor para insertar el mensaje de respuesta, es decir, de Activity a Fragment.
        respuesta = (EditText) findViewById(R.id.mainActivity_etxtRespuesta);

        //Boton para enviar la respuesta  Activity-Fragment al Fragment.
        btnRespuesta = (Button) findViewById(R.id.mainActivity_btnEnviarRespuesta);
        btnRespuesta.setOnClickListener(this);
    }


    /**
     * Interface para implementar y recoger el valor indicado en el Fragment
     *
     * @param name
     */

    @Override
    public void onFragmentInteraction(String name) {
        nombre.setText(name);
    }


    /**
     * Gestión eventos onClicks de la Activity
     *
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.mainActivity_btn:
                goFragment(BlankFragment.newInstance(""));
                break;
            case R.id.mainActivity_btnEnviarRespuesta:
                goFragment(BlankFragment.newInstance(respuesta.getText().toString()));
                break;
        }

    }

    private void goFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
    }
}
